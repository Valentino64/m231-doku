# m231-doku


## Installation von GIT

Ich habe im Fach Datenschutz und Datensicherheit gelernt wie ich GIT installiere. 
Ich habe im Internet GIT-downlooad geschrieben und es heruntergeladen.
Dann habe ich ein ordner erstellt der workspace heisst.


### Profil erstellen 

Ich habe dann ein Profil erstellt und einen Ordner erstellt.
Der Ordner heisst m231-doku.


###im Ordner hinzufügen 

ich musste dafür ins CMD gehen und den Ordner öffnen den ich vorher erstellt habe.
ich habe dan den https link im cmd in den workspace ordner hinzugefügt 
Mit "dir" kann man alle Ordner oder Dateien in einem Ordner sehen.
Mit "cd"+ ordner kann man den Ordner öffnen.
Mit "notepad"+ Dokument kann man ein Dokument mit Notepad öffnen

### Datenschutz und Datensicherheit

Ich habe herausgefunden, dass man sich nur an personen bezogenen Daten sich widmet.
Datensicherheit ist das man seine Daten sichert damit sie nicht verloren gehen. Datensicherheit bezieht sich eher auf die Daten als auf die Person.
Datenschutz heisst das man selber entscheiden muss wenn man die Daten weitergibt oder nicht. 
 Datenschutz handelt von der richtigen Verwendung persönlicher Informationen, während Datensicherheit sich darauf konzentriert, Daten vor Schäden und unbefugtem Zugriff zu schützen. Beide Konzepte sind in unserer vernetzten Welt von großer Bedeutung, um die Privatsphäre zu bewahren und sensible Informationen zu sichern.

 ## Backup

 ## Virenscanner und Datensicherung

Wenn ein Virus gefunden wird, bieten die meisten Virenscanner verschiedene Optionen. Manchmal können infizierte Dateien repariert werden, aber nicht immer. In einigen Fällen müssen Sie infizierte Dateien löschen, insbesondere wenn es sich um wichtige Systemdateien handelt. In solchen Situationen ist es hilfreich, eine funktionierende Datensicherung zu haben. Wenn Ihr Computer nicht mehr startet, kann ein Boot-Medium (Installations-DVD/CD) mit einem aktuellen Virenscanner und einem Programm zur Wiederherstellung der Datensicherung hilfreich sein.

## Programmeinstellungen

Da Würmer heutzutage weit verbreitet sind, sollten Sie in makrofähigen Programmen sicherheitstechnische Einstellungen vornehmen. Meistens können Sie einstellen, dass Makros erst nach Ihrer Zustimmung ausgeführt werden. Bei E-Mail-Programmen wie Outlook und Windows Mail können Sie Sicherheitszonen einrichten, vorzugsweise "eingeschränkte Sites". Sie erhöhen die Sicherheit, indem Sie die Anzeige von E-Mails im HTML-Format deaktivieren. Öffnen Sie niemals Anhänge in E-Mails direkt, sondern speichern Sie sie zuerst auf Ihrem Computer und überprüfen Sie sie mit einem aktuellen Virenscanner, bevor Sie sie öffnen.

## Updates

Viele Sicherheitsprobleme resultieren aus Softwarefehlern. Die meisten Softwarehersteller veröffentlichen regelmäßig Patches und Updates, um diese Fehler zu beheben. Sie können Ihre Computersicherheit gegen Viren und Würmer verbessern, indem Sie Patches für alle verwendete Software sofort installieren, sobald sie verfügbar sind. Für einige Programme sind häufiger Sicherheitslücken bekannt, wie zum Beispiel beim Internet Explorer oder Adobe Reader. In solchen Fällen sollten Sie alternative Anwendungen in Betracht ziehen, um diese Schwachstellen zu vermeiden. Sie könnten auch darüber nachdenken, auf ein weniger häufig angegriffenes Betriebssystem umzusteigen. Die Verbreitung von Windows lag im März 2021 bei etwa 76%, macOS bei etwa 16,5%, und Linux bei ungefähr 2% im Bereich von Workstations.

## – Backups

Um die Bedeutung der Datensicherung zu verdeutlichen, stellen Sie sich vor, Ihre Festplatte wäre plötzlich defekt, und all Ihre Daten wären verloren. Wie viele Ihrer persönlichen Daten könnten von nicht betroffenen Speichermedien wiederhergestellt werden? Haben Sie den Installationsdatenträger für das Betriebssystem? Haben Sie Zugriff auf die Seriennummern für die Installation Ihrer Software? Wenn Ihre Antwort nicht lautet: "Das ist ärgerlich, aber kein Problem", dann sollten Sie sich definitiv für das Thema Datensicherung und Wiederherstellung interessieren.

 ## Was sollte gesichert werden?

Dateien, die Sie selbst erstellen oder bearbeiten, gehen im Ernstfall unwiederbringlich verloren und sollten regelmäßig gesichert werden. Installierte Software ändert sich normalerweise selten und muss entsprechend seltener gesichert werden. Eine durchdachte Ablagestruktur erleichtert die Auswahl der relevanten Dateien, insbesondere wenn Sie eigene Ordner oder Laufwerke verwenden. In Netzwerken speichern Benutzer ihre Dateien oft auf zentralen Servern, was die Datensicherung an wenigen Stellen vereinfacht.

## Wo sollten Sie sichern?

Idealerweise sollte die Datensicherung auf einem externen Medium erfolgen, das physisch vom Computer getrennt ist, um vor Schadsoftware und menschlichen Fehlern zu schützen. Es gibt verschiedene Möglichkeiten, dies zu tun:

Auf einer anderen Festplatte, die in einem Wechselrahmen oder einem externen USB-Gehäuse steckt.
Auf einem USB-Stick oder einer Speicherkarte.
Auf Blu-ray-Discs, DVDs oder CDs.
Auf Sicherungsbändern.
In der Cloud.
Unabhängig von Ihrem gewählten Medium sollten Sie die Haltbarkeit, Lagerung und den Erfolg Ihrer Datensicherung berücksichtigen. Eine regelmäßige Überprüfung und Testwiederherstellung sind ebenfalls wichtig.

## Wie oft sollten Sie sichern?

Die Häufigkeit der Datensicherung hängt davon ab, wie oft sich Ihre Daten ändern und wie wichtig sie sind. Für Unternehmen mit vielen täglichen Transaktionen sind komplexere Sicherungsstrategien erforderlich als für den persönlichen Gebrauch zu Hause.

## Datensicherungsstrategien

Eine gängige Datensicherungsstrategie für kleinere Unternehmen sieht vor, dass alle Daten auf einem zentralen Dateiserver gespeichert werden. Täglich werden die Daten mithilfe eines Sicherungsgeräts (z. B. Streamer) auf Magnetbänder gesichert. Diese Bänder sind nach Wochentagen beschriftet, sodass Sie im Notfall auf die Daten der letzten Tage zurückgreifen können. Eine regelmäßige Sicherung und Überprüfung der Daten ist entscheidend, um sicherzustellen, dass sie im Ernstfall wiederhergestellt werden können.

Die Wahl zwischen inkrementeller und differenzieller Sicherung unter der Woche hängt von der Datenmenge und den Anforderungen an die Wiederherstellung ab. Beide Methoden nutzen das Archivbit, um neue und geänderte Dateien zu kennzeichnen. Eine Vollsicherung erfolgt normalerweise am Ende der Woche, um alle Archivbits zu löschen.

Denken Sie bei der Datensicherung auch an die Haltbarkeit, Lagerung und den Schutz Ihrer Daten vor Diebstahl oder Katastrophen. Eine sorgfältige Planung und Umsetzung der Datensicherung ist entscheidend, um Ihre wertvollen Informationen zu schützen.


###Welche Probleme bringt die Digitalisierung mit sich?

Die Digitalisierung wirft einige wichtige Probleme auf. Dazu gehören Widerstand gegen Veränderungen, Schwierigkeiten bei der Einführung und Organisation digitaler Prozesse sowie der hohe Zeitaufwand und die hohen Kosten, die mit dem Umstieg auf digitale Technologien verbunden sind.

##Grundlagen des Datenschutzes

Die Hauptregel im Datenschutz, speziell in der Datenschutz-Grundverordnung (DSGVO), besagt, dass die Nutzung von persönlichen Informationen grundsätzlich verboten ist. Sie ist nur in Ausnahmefällen erlaubt, wenn bestimmte Bedingungen aus der DSGVO erfüllt sind.

Erklärung von Begriffen:

##Datenschutz:

Datenschutz bedeutet, persönliche Informationen vor Missbrauch zu schützen und das Recht auf persönliche Kontrolle über diese Informationen zu bewahren.

##Datensicherheit:

Datensicherheit bezieht sich auf die Praktiken, um digitale Informationen vor unbefugtem Zugriff, Beschädigung oder Diebstahl zu schützen, während sie verwendet werden.

Besonders schützenswerte Daten:

##Welche Informationen gelten als besonders schützenswert? Dazu gehören:

Informationen über religiöse, weltanschauliche, politische oder gewerkschaftliche Ansichten oder Aktivitäten.
Gesundheitsdaten und private Informationen, wie zum Beispiel Informationen über Ihre persönliche Lebensweise oder ethnische Zugehörigkeit.
Informationen über Sozialhilfemaßnahmen.


https://gitlab.com/Valentino64/m231-doku/-/blob/main/backup.md?ref_type=heads











