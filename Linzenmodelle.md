# Lizenzmodelle

### Aufgabe 1
Eine Lizenz ist eine art gehnemigung für dinge die eigentlich beschränkt sind.
Es ist sozusagen ein Okay, das du etwas bestimmtes nutzen und verwenden kannst.

### Aufgabe 2
Lizenze werden hergestellt wenn leute etwas besonderes machen das nicht von anderen leuten geklaut werden.Es wird sogar manchmal gesetzlich gesagt das man eine Lizenz dafür machen soll. Kurz gesagt, Lizenzen regeln, wer was verwenden kann und unter welchen Bedingungen.

### Aufgabe 3
Es gibt für viele dinge Lizenzen. Hier sind paar beispiele.
Musik: Musik von berühmten Personen werden meistens Lizensiert
Filme: Alle filme müssen auch eine Lizenz haben.
Marken:Eher teuer und berühmte Marken werden lizensiert.
Sportvereine:Auch bei sportvereine gibt es Lizenzen.

### Aufgabe 4

Proprietäre Lizenz:
Unternehmen behalten die Kontrolle über beispielsweise den Code.

Open-Source-Lizenz:
Anwender dürfen Änderungen am Quellcode der Software vornehmen.

Untergeordnete Lizenzmodelle:

Dauerlizenz:
Nach einem einmaligen Kauf hat man dauerhaften Zugriff auf die Software.

Abonnement-basierte Lizenz:
Nach einer Zahlung hat man für einen bestimmten Zeitraum Zugriff auf die Software und muss erneut zahlen, um den Zugriff zu verlängern.

Floating-Lizenz:
Es können nur eine begrenzte Anzahl von Lizenzen gleichzeitig verwendet werden, unabhängig von der Benutzeridentität.

Node-Locked-Lizenz:
Eine Lizenz wird einem spezifischen Gerät zugewiesen.

Public-Domain-Lizenz:
Es gibt keine Einschränkungen bezüglich der Verwendung der Software.

Copyleft-Lizenz:
Bei Open-Source-Software müssen Änderungen am Code unter denselben Lizenzbedingungen veröffentlicht werden.

Windows CAL:
Zugriff auf Anwendungen erfolgt nur von einem Server aus, wofür eine Berechtigung zur Verbindung mit dem Server erforderlich ist.

### Aufgabe 5
Open Source bedeutet, dass der Code einer Software für jeden öffentlich zugänglich ist. Mit dieser Art von Software kann man Folgendes machen:

Nutzen: Man kann die Software kostenlos verwenden.

Anpassen: Entwickler können den Code verändern, um die Software an ihre Bedürfnisse anzupassen.

Weiterentwicklung: Die Software wird von einer Gemeinschaft von Entwicklern ständig verbessert.

Weitergeben: Man kann die Software anderen kostenlos zur Verfügung stellen.

Sicherheit: Da der Code offen ist, kann man prüfen, ob die Software sicher ist.

Beispiele für Open-Source-Software sind Linux, Firefox, und WordPress. Open Source hat in vielen Bereichen, wie Softwareentwicklung, Bildung und Unternehmen, einen großen Einfluss.

#### Aufgabe 6
"Copyright" schützt die Urheberrechte und ermöglicht es dem Urheber, die Verwendung seines Werks zu kontrollieren. "Copyleft" ist eine Philosophie, die das Urheberrecht nutzt, um sicherzustellen, dass ein Werk und seine Abwandlungen für alle offen und frei bleiben. Werke unter "Copyleft" müssen unter denselben Bedingungen geteilt werden.

### Aufgabe 7
wenn man im App-Store eine zu bezahlende App heunterlädt und installiert?/im App-Store eine Gratis-App heunterlädt und installiert? *Haben Sie die Software bezahlt, als Sie ihr aktuelles Smartphone gekauft/bekommen haben? Wenn Sie es nicht wissen, ob Sie extra bezahlt haben, wie und wann bezahlen Sie? im App-Store eine zu bezahlende App heunterlädt und installiert? proprietären Lizenz, Dauerlizenz im App-Store eine Gratis-App heunterlädt und installiert? proprietären Lizenz, Dauerlizenz
