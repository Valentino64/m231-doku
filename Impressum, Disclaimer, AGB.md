### Impressum, Disclaimer, AGB

## Aufgabe 1 
Impressum
Deutsch=Anbieterkennzeichnung
Englisch=imprint
Französisch=imprimer
Italienisch=impronta
Spanisch=imprimir
Albanisch=gjurmë

## Aufgabe 2
Ein Impressum ist eine Seite, die häufig auf Websites oder in gedruckten Medien zu finden ist und als Über uns Seite funktioniert. Sie enthält wesentliche Informationen über den Betreiber oder Herausgeber wie beispielsweise den Namen, die Adresse und die Kontaktdaten. Dies dient oft der gesetzlichen Vorgabe, um sicherzustellen, dass die Verantwortlichen leicht identifiziert werden können.

## Aufgabe 3
In der Schweiz benötigen gewerbliche Websites ein Impressum. Private Websites ohne kommerzielle Absichten brauchen normalerweise keins. Online-Shops und kommerzielle Plattformen müssen immer eins haben.

## Aufgabe 4
In Deutschland und der EU ist es erforderlich, ein Impressum auf Ihrer Website oder in den sozialen Medien anzugeben, wenn Sie geschäftliche Zwecke verfolgen. Im Impressum sollten wichtige Informationen wie Ihr Name, Ihre Adresse und Kontaktdaten enthalten sein.
In der Schweiz gelten ähnliche Bestimmungen, obwohl die genauen Anforderungen leicht variieren können. Es ist entscheidend, die aktuellen Gesetze und Vorschriften zu berücksichtigen, um sicherzustellen, dass Ihr Impressum den erforderlichen Standards entspricht.

## Aufgabe 5 
Disclaimer
Deutsch= Haftungsausschluss
Englisch= exemption
Französisch= Clause de non-responsabilité
Italienisch= Disclaimer
Spanisch= Descargo de responsabilidad
Albanisch= Mohim përgjegjësie

## Aufgabe 6
Der Hauptzweck eines Disclaimers besteht darin, rechtliche Klarheit zu schaffen und den Website-Betreiber vor möglichen rechtlichen Schwierigkeiten zu schützen. Damit man auch sagen das der Website-Betreiber nicht für Fehler oder Schaden verantwortlich ist. Die genaue Ausgestaltung kann je nach den geltenden Gesetzen in der jeweiligen Region unterschiedlich sein. 

## Aufgabe 7
AGB bedeutet Allgemeine Geschäftsbedingung, das sind vorgefertigte Regeln von einem Vertrag. In den Regeln steht zum Beispiel wie die Beziehung zwischen Kunde und dem Unternehmen gilt. Mann sollte deshalb die AGB immer durchlesen, bevor man einen Vertrag unterschreibt.

## Aufgabe 8
Das habe ich im Internet gefunden, das ist eine AGB Muster Seite wo man mehrere Muster von AGBs seht und sich inspirieren lassen kann.
https://www.schweizer-vertraege.ch/Suchbegriff/60-AGB

